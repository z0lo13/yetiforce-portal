<?php
/**
 * Main configuration file
 */
$config = [];
$config['crmPath'] = 'https://portal.hmcvanlines.ca/';
$config['portalPath'] = 'https://yeti.portal/';
$config['language'] = 'en_us';
$config['theme'] = 'default';
$config['defaultModule'] = 'HelpDesk';
$config['timezone'] = 'America/Toronto';
$config['languages'] = [
	'en_us' => 'English',
	'pl_pl' => 'Polski',
];
$config['listMaxEntriesFromApi'] = 50;
// Available record display options in listview for datatable element - [[values],[labels]]
$config['listEntriesPerPage'] = [[10, 25, 50, 100], [10, 25, 50, 100]];
$config['minScripts'] = false;
$config['debugApi'] = true;
$config['debugConsole'] = true;
$config['logs'] = true;
// Security
$config['apiKey'] = 'VaY09KjFA80GNfTrAJWJBdsvSDshFRHm';
$config['serverName'] = 'YetiPortal';
$config['serverPass'] = 'Soaks2709';
$config['encryptDataTransfer'] = false;
$config['privateKey'] = 'config/private.key';
$config['publicKey'] = 'config/public.key';
$config['logo'] = 'layouts/Default/skins/images/logo.png';
/** If timezone is configured, try to set it */
if (isset($config['timezone']) && function_exists('date_default_timezone_set')) {
	@date_default_timezone_set($config['timezone']);
}
